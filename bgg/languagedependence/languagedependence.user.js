// ==UserScript==
// @name BGG Language dependence on game info panel.
// @namespace tequila_j-script
// @version    1.3.1
// @description  Shows language dependence in BGG gameplay bar (boardgamegeek.com).
// @match      http://*.boardgamegeek.com/*
// @match      http://boardgamegeek.com/*
// @match      https://*.boardgamegeek.com/*
// @match      https://boardgamegeek.com/*
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js
// @grant    GM_addStyle
// @run-at document-end
// ==/UserScript==


angular.element(document).ready(function () {

  var point = $('[item-poll-button="languagedependence"] > span')[0];
  console.log($(point).html());
  var scope = angular.element(point).scope();

  console.log(scope);

  var gameplayBar = $('ul.gameplay');

  //var featuresBar = $('div.game-description-secondary').find('ul.features > li.feature:first').find(":contains('ADDITIONAL SUGGESTIONS'");
  var featuresBar = $('ul.features').find("div.feature-title:contains('Language Dependence')").parent();
  
  var ldTextNode = angular.copy($(featuresBar).find('.feature-title'));
  
  var ldValueNode = angular.copy($(featuresBar).find('.feature-description'));
  
  var clickEventForwarderDest = $(featuresBar).find('.feature-description').children('span');
  
  //var ldButton = ldValueNode.find("button").removeClass("feature-action-icon");
  
  var ldHelp = ldTextNode.find("a").addClass("c-icon fs-xs");
    
  var newGamePlayItem = gameplayBar.children('li:first').clone();
  
  //this text is a copy of the small stats button, but btn-empty was replaced by btn-link and feature-action-icon was removed
  //ng-bind-html="geekitemctrl.geekitem.data.item.polls.languagedependence|to_trusted"
  var ldButton = $(`
  <button type="button" 
    class="btn btn-xs btn-link" 
    uib-tooltip="View poll and results" tooltip-popup-delay="500" tooltip-append-to-body="true"
    >Language Dependence
      <span class="glyphicon glyphicon-stats"></span>
  </button>');
`);
  
  //var ldSubNewContainer = ldValueNode.children('span').first().empty().append(ldButton);
  //var ldSubNewContainer = ldButton;
  var ldSubNewContainer = $('<span item-poll-button="languagedependence" poll-view="results"></span>').append(ldButton);
  
  //bind click event to original source (they source is changed)
  if (clickEventForwarderDest !== undefined) {
    ldSubNewContainer.on('click', function() {
	    clickEventForwarderDest.trigger('click');
    });
  }
  
  //creating a copy of the info list
  var newGamePlayItem = gameplayBar.children('li:first').clone();
  newGamePlayItem.children('.gameplay-item-primary').empty().text(GEEK.geekitemPreload["item"]["polls"]["languagedependence"]);
  newGamePlayItem.children('.gameplay-item-secondary').empty().append(ldSubNewContainer).append(ldHelp);
  
  angular.bootstrap(newGamePlayItem, ['ui.bootstrap']); //balloons
  gameplayBar.append(newGamePlayItem);

  
  
/** this does not work - only if debug is enabled 
 
  angular.element(document).injector().invoke(function($compile) {
           // Get the AngularJS scope we want our fragment to see
          var scope = angular.element(newGamePlayItem).scope();

          console.log("Scope:" + scope);

           // Pass our fragment content to $compile,
           // and call the function that $compile returns with the scope.
           var compiledContent = $compile(newGamePlayItem)(scope);

           gameplayBar.append(compiledContent);

  });
*/  

});

